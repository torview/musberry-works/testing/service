const assert = require('assert');
const {Given, When, Then} = require('@cucumber/cucumber');
const axios = require("axios");
const {TestFixtures} = require("../../fixtures");

const config = {
    services: {
        cotton: (process.env.COTTON_SERVICE_URL || "http://localhost:3101"),
        ui: (process.env.UI_URL || "http://localhost:3100"),
        wool: (process.env.WOOL_SERVICE_URL || "http://localhost:3102")
    },
}
const getUrl = (service, path) => `${config.services[service]}${path}`

//TODO - Refactor to use world
let response;

Given('the standard test data set has been run in', async function () {
    return await Promise.all([
            ...TestFixtures.cotton.standard.map(data => axios.post(`${config.services.cotton}/cotton`, data)),
            ...TestFixtures.wool.standard.map(data => axios.post(`${config.services.wool}/wool`, data))
        ]
    );
});

When('I send a GET request to {string} on the {string}', async function (path, service) {
    response = await axios.get(getUrl(service, path));
});

Then('the page should contain the text {string}', function (string) {
    assert.equal(response.data.includes(string), true)
});

Then('the response array should equal', function (expectedString) {
    const expected = JSON.parse(expectedString);
    const sortById = (a, b) => {
        if (a.id > b.id) {
            return -1;
        } else if (a.id < b.id) {
            return 1;
        }
        return 0;
    }
    assert.deepEqual(response.data.sort(sortById), expected.sort(sortById))
});
