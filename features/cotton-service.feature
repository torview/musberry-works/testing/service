@cotton-service
Feature: Cotton Service

  Scenario: GET /cotton returns all cotton items
    Given the standard test data set has been run in
    When I send a GET request to "/cotton" on the "cotton"
    Then the response array should equal
    """
    [
      { "id": 1, "type": "cotton" },
      { "id": 2, "type": "cotton" },
      { "id": 3, "type": "cotton" }
    ]
    """
