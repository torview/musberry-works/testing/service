@wool-service
Feature: Wool Service

  Scenario: GET /wool returns all wool items
    Given the standard test data set has been run in
    When I send a GET request to "/wool" on the "wool"
    Then the response array should equal
    """
    [
      { "id": 1, "type": "wool" },
      { "id": 2, "type": "wool" },
      { "id": 3, "type": "wool" }
    ]
    """
