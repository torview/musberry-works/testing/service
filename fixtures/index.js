const {cotton} = require('./cotton-service')
const {wool} = require('./wool-service')

module.exports = {
    TestFixtures: {
        cotton,
        wool
    }
}
