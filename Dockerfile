FROM node:lts-alpine

WORKDIR /opt/service-testing
COPY . /opt/service-testing

#Installing app
RUN npm ci

#Security requirement
USER node

#Entry point
CMD ["npm", "run", "test" ]

